DOCKER_COMPOSE = docker-compose
EXEC_PHP       = $(DOCKER_COMPOSE) exec -T php
COMPOSER       = $(EXEC_PHP) composer
EXEC_BASH	   = $(DOCKER_COMPOSE) exec php /bin/bash

start:
	$(DOCKER_COMPOSE) up --build --remove-orphans --force-recreate --detach

stop:
	$(DOCKER_COMPOSE) stop

down:
	$(DOCKER_COMPOSE) down

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

clean: kill
	rm -rf var vendor

reinstall: clean

vendor: ./app/composer.json ./app/composer.lock
	$(COMPOSER) install

.PHONY: install start stop kill clean reinstall