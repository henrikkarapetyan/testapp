<?php
namespace app\utils;

enum MessageTypeEnum: string
{
    case NEW_MESSAGE = 'new message';

    case SUCCESS_SEND = 'success send';

    case MESSAGE_STATUS_CHANGE = 'message status change';

    case NEW_NOTE = 'new note';
}