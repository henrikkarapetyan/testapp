<?php

namespace app\exchanges;

use app\helpers\AutoAnswerSender;
use mikemadisonweb\rabbitmq\components\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

use Yii;

class MessageConsumer implements ConsumerInterface
{

    public function execute(AMQPMessage $msg)
    {
        $model = json_decode($msg->body);

        if ($model->payload->isItClient) {
            Yii::info(sprintf(
                "Client => %s \n Question => %s",
                $model->payload->client->name,
                $model->payload->text
            ), CLIENTS_LOG);
            AutoAnswerSender::detectKeywordAndSendAutoAnswer($model->payload->text, $model->payload->dialogId);
        } else {
            Yii::info(sprintf(
                "Operator => %s \n Answer => %s",
                $model->payload->operator->name,
                $model->payload->text
            ), OPERATORS_LOG);
        }

        return ConsumerInterface::MSG_ACK;
    }
}