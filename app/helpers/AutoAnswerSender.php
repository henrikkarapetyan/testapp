<?php

namespace app\helpers;

use Yii;
use yii\httpclient\Client;

class AutoAnswerSender
{
    public static function detectKeywordAndSendAutoAnswer(string $text, string $dialogId): void
    {
        if (isset(Yii::$app->params['reservedWord'])) {
            $reservedWord = Yii::$app->params['reservedWord'];
            preg_match("/$reservedWord/", $text, $matches);

            if ($matches) {
                $client = new Client();
                $response = $client->createRequest()
                    ->setMethod('POST')
                    ->setUrl(sprintf('%s/message/send', Yii::$app->params['teletypeDomain']))
                    ->addHeaders([
                        'X-Auth-Token' => Yii::$app->params['teletypeAccessToken']
                    ])
                    ->setData(['dialogId' => $dialogId, 'text' => Yii::$app->params['answerForReservedWord']])
                    ->send();
                if (!$response->isOk) {
                    Yii::error("Something went wrong => %s", $response->getContent());
                }
            }
        }

    }
}