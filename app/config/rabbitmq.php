<?php

use app\exchanges\MessageConsumer;
use mikemadisonweb\rabbitmq\Configuration;

return [
    'class' => Configuration::class,
    'connections' => [
        [
            'host' => 'mq',
            'port' => '5672',
            'user' => 'rmuser',
            'password' => 'rmpassword',
            'vhost' => '/',
        ]
    ],
    'exchanges' => [
        [
            'name' => 'messages',
            'type' => 'direct'
        ],
    ],
    'queues' => [
        [
            'name' => 'messagesQueue',
            'durable' => true,
            'auto_delete' => false,
        ]
    ],
    'bindings' => [
        [
            'queue' => 'messagesQueue',
            'exchange' => 'messages',
            'routing_keys' => ['messages'],
        ],
    ],
    'producers' => [
        [
            'name' => 'messageProducer',
        ],
    ],
    'consumers' => [
        [
            'name' => 'messages',
            'callbacks' => [
                'messagesQueue' => MessageConsumer::class,
            ],
        ],
    ],
];