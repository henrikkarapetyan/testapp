<?php
defined('OPERATORS_LOG') or define('OPERATORS_LOG', 'operators_log');
defined('CLIENTS_LOG') or define('CLIENTS_LOG', 'clients_logs');

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'reservedWord' => 'ping',
    'answerForReservedWord' => 'pong',
    'teletypeDomain' => 'https://api.teletype.app/public/api/v1',
    'teletypeAccessToken' => 'DQtf5C2T_1na4aoxUd_T5T_UWdwKAs_kDMHNZCePxGl5kUqnRZAx5pw5KD72ZVnO',
];