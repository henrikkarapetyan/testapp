<?php

return [
    'traceLevel' => YII_DEBUG ? 3 : 0,
    'flushInterval' => 1,
    'targets' => [
        [
            'class' => 'yii\log\FileTarget',
            'levels' => ['error', 'warning'],
        ],
        [
            'class' => 'yii\log\FileTarget',
            'categories' => [OPERATORS_LOG],
            'exportInterval' => 1,
            'levels' => ['info'],
            'logFile' => '@app/runtime/logs/operators.log',
            'maxFileSize' => 1024 * 2,
            'maxLogFiles' => 50,
            'logVars' => [],

        ],
        [
            'class' => 'yii\log\FileTarget',
            'categories' => [CLIENTS_LOG],
            'exportInterval' => 1,
            'levels' => ['info'],
            'logFile' => '@app/runtime/logs/clients.log',
            'maxFileSize' => 1024 * 2,
            'maxLogFiles' => 50,
            'logVars' => [],

        ]
    ],
];