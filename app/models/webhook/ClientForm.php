<?php

namespace app\models\webhook;

use yii\base\DynamicModel;

class ClientForm extends DynamicModel implements \Stringable
{

    public string $id;
    public string $name;
    public string $avatar;
    public ?string $phone;

    public function formName(): string
    {
        return '';
    }

    public function rules(): array
    {
        return [
            [['id', 'name', 'avatar'], 'required'],
            [['id', 'name', 'avatar'], 'string'],
            [['phone'], 'udokmeci\yii2PhoneValidator\PhoneValidator', 'countryAttribute' => 'country_code', 'strict' => false, 'format' => true, 'skipOnEmpty' => true],
        ];
    }


    public function __toString(): string
    {
        return sprintf('id: %s name: %s avatar: %s phone: %s', $this->id, $this->name, $this->avatar, $this->phone);
    }
}