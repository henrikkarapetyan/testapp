<?php

namespace app\models\webhook;

use yii\base\DynamicModel;

class OperatorForm extends DynamicModel implements \Stringable
{
    public string $id;
    public string $name;
    public ?string $avatar;


    public function rules(): array
    {
        return [
            [['id', 'name'], 'required'],
            [['id', 'name'], 'string', 'min' => 1, 'max' => 255],
            [['avatar'], 'string', 'skipOnEmpty' => true],
        ];
    }

    public function formName(): string
    {
        return '';
    }

    public function __toString(): string
    {
        return sprintf('id: %s, name: %s, avatar: %s', $this->id, $this->name, $this->avatar);
    }
}