<?php

namespace app\models\webhook;

use yii\base\DynamicModel;
use yii\helpers\Json;

class MessageForm extends DynamicModel
{
    public string $name;

    /**
     * @var string|PayloadForm
     */
    public $payload;

    public function formName(): string
    {
        return '';
    }

    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'min' => 1, 'max' => 255],
            [['payload'], 'validatePayload', 'skipOnEmpty' => false],
        ];
    }

    public function validatePayload(): bool
    {
        if (!is_string($this->payload)) {
            $this->addError('payload', 'payload must be a string');
            return false;
        }

        $data = Json::decode($this->payload);

        if (!isset($data['message'])) {
            $this->addError('payload', 'message attribute is missing');
            return false;
        }

        $payloadForm = new PayloadForm();

        $payloadForm->load($data['message']);
        $this->payload = $payloadForm;

        if (!$payloadForm->validate()) {
            $this->addErrors($payloadForm->getErrors());
            return false;
        }

        return true;
    }
}