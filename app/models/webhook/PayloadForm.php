<?php

namespace app\models\webhook;

use yii\base\DynamicModel;

class PayloadForm extends DynamicModel
{

    public string $id;
    public string $dialogId;
    public string $sessionId;
    public string $text;
    public int $provider;
    public bool $isItClient;
    public bool $seen;
    public int $status;
    public bool $isGroupChat;
    public null|array|ClientForm $client;
    public null|array|OperatorForm $operator;

    public function formName(): string
    {
        return '';
    }

    public function rules(): array
    {
        return [
            [['id', 'dialogId', 'sessionId', 'text'], 'required'],
            [['id', 'dialogId', 'sessionId', 'text'], 'string', 'min' => 1],
            [['isItClient', 'isGroupChat', 'seen'], 'boolean'],
            [['provider', 'status'], 'integer'],
            ['client', 'validateClient', 'when' => function ($model) {
                return $model->isItClient;
            }, 'skipOnEmpty' => true,],
            ['operator', 'validateOperator', 'when' => function ($model) {
                return !$model->isItClient;
            }, 'skipOnEmpty' => true,]];
    }

    public function validateClient(): bool
    {
        $clientForm = new ClientForm();

        $clientForm->load($this->client);
        $this->client = $clientForm;
        return $clientForm->validate();
    }

    public function validateOperator(): bool
    {
        $operatorForm = new OperatorForm();

        $operatorForm->load($this->operator);
        $this->operator = $operatorForm;

        return $operatorForm->validate();
    }
}