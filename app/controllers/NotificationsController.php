<?php

namespace app\controllers;

use app\helpers\AutoAnswerSender;
use app\models\webhook\MessageForm;
use app\utils\MessageTypeEnum;
use mikemadisonweb\rabbitmq\components\Producer;
use Yii;
use yii\rest\Controller;

class NotificationsController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return array
     */
    public function actionIndex(): array
    {
        $model = new MessageForm();
        $model->load(Yii::$app->request->post());

        if (!$model->validate()) {
            return $model->getErrors();
        }



        match ($model->name) {
            MessageTypeEnum::NEW_MESSAGE->value, MessageTypeEnum::SUCCESS_SEND->value => $this->publishMessage($model),
            default => [] //we're only tracking `new message` and `success send` message types
        };

        return ['message' => 'sended'];

    }

    private function publishMessage(MessageForm $model): void
    {
        /** @var Producer $producer */
        $producer = \Yii::$container->get(sprintf('rabbit_mq.producer.%s', 'messageProducer'));
        $msg = json_encode($model);

        $producer->publish($msg, 'messages','messages');
    }


}